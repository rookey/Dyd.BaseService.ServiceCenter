﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ServiceCenter.Domain.Model;

namespace Dyd.BaseService.ServiceCenter.Domain.Dal
{
    //tb_protocolversion
    public partial class tb_protocolversion_dal
    {
        #region C

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_protocolversion model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into tb_protocolversion(");
            strSql.Append("serviceid,version,interfaceversion,createtime,protocoljson");
            strSql.Append(") values (");
            strSql.Append("@serviceid,@version,@interfaceversion,@createtime,@protocoljson");
            strSql.Append(") ");
            strSql.Append(";select @@IDENTITY");
            par.Add(new ProcedureParameter("@serviceid", model.serviceid));
            par.Add(new ProcedureParameter("@version", model.version));
            par.Add(new ProcedureParameter("@interfaceversion", model.interfaceversion));
            par.Add(new ProcedureParameter("@createtime", model.createtime));
            par.Add(new ProcedureParameter("@protocoljson", model.protocoljson));


            object obj = conn.ExecuteSql(strSql.ToString(), par);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_protocolversion model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tb_protocolversion set ");

            strSql.Append(" serviceid = @serviceid , ");
            strSql.Append(" version = @version , ");
            strSql.Append(" interfaceversion = @interfaceversion , ");
            strSql.Append(" createtime = @createtime , ");
            strSql.Append(" protocoljson = @protocoljson  ");
            strSql.Append(" where id=@id ");

            par.Add(new ProcedureParameter("@id", model.id));
            par.Add(new ProcedureParameter("@serviceid", model.serviceid));
            par.Add(new ProcedureParameter("@version", model.version));
            par.Add(new ProcedureParameter("@interfaceversion", model.interfaceversion));
            par.Add(new ProcedureParameter("@createtime", model.createtime));
            par.Add(new ProcedureParameter("@protocoljson", model.protocoljson));

            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tb_protocolversion ");
            strSql.Append(" where id=@id");
            par.Add(new ProcedureParameter("@id", id));
            int rows = conn.ExecuteSql(strSql.ToString(), par);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Q
        /// <summary>
        /// 重复规则
        /// </summary>
        public bool IsExists(DbConn conn, tb_protocolversion model)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_category s where 1=1");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_protocolversion Get(DbConn conn, int id)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, serviceid, version, interfaceversion, createtime, protocoljson  ");
            strSql.Append(" from tb_protocolversion ");
            strSql.Append(" where id=@id");
            par.Add(new ProcedureParameter("@id", id));
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, strSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_protocolversion Get(DbConn conn, int serviceid, decimal version)
        {
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id, serviceid, version, interfaceversion, createtime, protocoljson  ");
            strSql.Append(" from tb_protocolversion ");
            strSql.Append(" where serviceid=@serviceid and [version]=@version");
            par.Add(new ProcedureParameter("@serviceid", serviceid));
            par.Add(new ProcedureParameter("@version", version));
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, strSql.ToString(), par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="search">查询实体</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        public IList<tb_protocolversion> GetPageList(DbConn conn, tb_protocolversion_search search, out int totalCount)
        {
            totalCount = 0;
            List<ProcedureParameter> par = new List<ProcedureParameter>();
            IList<tb_protocolversion> list = new List<tb_protocolversion>();
            long startIndex = (search.Pno - 1) * search.PageSize + 1;
            long endIndex = startIndex + search.PageSize - 1;
            par.Add(new ProcedureParameter("startIndex", startIndex));
            par.Add(new ProcedureParameter("endIndex", endIndex));

            StringBuilder baseSql = new StringBuilder();
            string sqlWhere = " where 1=1 ";
            baseSql.Append("select row_number() over(order by id desc) as rownum,");
            #region Query
            if (search.serviceid > 0)
            {
                sqlWhere += " and serviceid=@serviceid";
                par.Add(new ProcedureParameter("serviceid", search.serviceid));
            }

            if (!string.IsNullOrWhiteSpace(search.key))
            {
                sqlWhere += " and protocoljson like '%'+ @key +'%'";
                par.Add(new ProcedureParameter("key", ProcParType.NVarchar, 500, search.key));
            }

            if (search.version > 0)
            {
                sqlWhere += " and [version] = @version";
                par.Add(new ProcedureParameter("version", ProcParType.Int32, 4, search.version));
            }

            #endregion
            baseSql.Append(" * ");
            baseSql.Append(" FROM tb_protocolversion with(nolock)");

            string execSql = "select * from(" + baseSql.ToString() + sqlWhere + ")  as s where rownum between @startIndex and @endIndex";
            string countSql = "select count(1) from tb_protocolversion with(nolock)" + sqlWhere;
            object obj = conn.ExecuteScalar(countSql, par);
            if (obj != DBNull.Value && obj != null)
            {
                totalCount = LibConvert.ObjToInt(obj);
            }
            DataTable dt = conn.SqlToDataTable(execSql, par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        #endregion

        #region private
        public tb_protocolversion CreateModel(DataRow dr)
        {
            var model = new tb_protocolversion();
            if (dr.Table.Columns.Contains("id") && dr["id"].ToString() != "")
            {
                model.id = int.Parse(dr["id"].ToString());
            }
            if (dr.Table.Columns.Contains("serviceid") && dr["serviceid"].ToString() != "")
            {
                model.serviceid = int.Parse(dr["serviceid"].ToString());
            }
            if (dr.Table.Columns.Contains("version") && dr["version"].ToString() != "")
            {
                model.version = int.Parse(dr["version"].ToString());
            }
            if (dr.Table.Columns.Contains("interfaceversion") && dr["interfaceversion"].ToString() != "")
            {
                model.interfaceversion = decimal.Parse(dr["interfaceversion"].ToString());
            }
            if (dr.Table.Columns.Contains("createtime") && dr["createtime"].ToString() != "")
            {
                model.createtime = DateTime.Parse(dr["createtime"].ToString());
            }
            if (dr.Table.Columns.Contains("protocoljson") && dr["protocoljson"].ToString() != "")
            {
                model.protocoljson = dr["protocoljson"].ToString();
            }

            return model;
        }
        #endregion
    }
}
﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_node
    public class tb_node
    {

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// 服务id
        /// </summary>
        [Display(Name = "服务id")]
        public int serviceid { get; set; }
        /// <summary>
        /// 服务命名空间（服务唯一标示）
        /// </summary>
        [Display(Name = "服务命名空间")]
        public string servicenamespace { get; set; }
        /// <summary>
        /// 会话id
        /// </summary>
        [Display(Name = "会话id")]
        public long sessionid { get; set; }
        /// <summary>
        /// ip地址
        /// </summary>
        [Display(Name = "ip地址")]
        public string ip { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        [Display(Name = "端口")]
        public int port { get; set; }
        /// <summary>
        /// 运行状态
        /// </summary>
        [Display(Name = "状态")]
        public int runstate { get; set; }
        /// <summary>
        /// 节点心跳时间
        /// </summary>
        [Display(Name = "心跳时间")]
        public DateTime nodeheartbeattime { get; set; }
        /// <summary>
        /// 权重值
        /// </summary>
        [Display(Name = "权重值")]
        public int boostpercent { get; set; }
        /// <summary>
        /// 错误数量
        /// </summary>
        [Display(Name = "错误数")]
        public long errorcount { get; set; }
        /// <summary>
        /// 连接数量
        /// </summary>
        [Display(Name = "调用次数")]
        public long visitcount { get; set; }
        /// <summary>
        /// 访问数量
        /// </summary>
        [Display(Name = "当前连接数")]
        public long connectioncount { get; set; }
        /// <summary>
        /// 线程数
        /// </summary>
        [Display(Name = "线程数")]
        public int processthreadcount { get; set; }
        /// <summary>
        /// cpu时间
        /// </summary>
        [Display(Name = "cpu时间")]
        public decimal processcpuper { get; set; }
        /// <summary>
        /// 内存大小
        /// </summary>
        [Display(Name = "内存大小")]
        public decimal memorysize { get; set; }
        /// <summary>
        /// 文件大小
        /// </summary>
        [Display(Name = "文件大小")]
        public decimal filesize { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }
        /// <summary>
        /// 接口版本号
        /// </summary>
        [Display(Name = "接口版本号")]
        public decimal interfaceversion { get; set; }
        /// <summary>
        /// 协议json
        /// </summary>
        [Display(Name = "协议json")]
        public string protocoljson { get; set; }

    }

    //tb_node
    public class tb_node_search : BaseSearch
    {
        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// 服务id
        /// </summary>
        [Display(Name = "服务id")]
        public int serviceid { get; set; }
        /// <summary>
        /// 会话id
        /// </summary>
        [Display(Name = "会话id")]
        public long sessionid { get; set; }
        /// <summary>
        /// ip地址
        /// </summary>
        [Display(Name = "ip地址")]
        public string ip { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        [Display(Name = "端口")]
        public int port { get; set; }
        /// <summary>
        /// 运行状态
        /// </summary>
        [Display(Name = "运行状态")]
        public int runstate { get; set; }
        /// <summary>
        /// 节点心跳时间
        /// </summary>
        [Display(Name = "节点心跳时间")]
        public DateTime nodeheartbeattime { get; set; }
        /// <summary>
        /// 权重值
        /// </summary>
        [Display(Name = "权重值")]
        public int boostpercent { get; set; }
        /// <summary>
        /// 错误数量
        /// </summary>
        [Display(Name = "错误数量")]
        public long errorcount { get; set; }
        /// <summary>
        /// 访问数量
        /// </summary>
        [Display(Name = "访问数量")]
        public long connectioncount { get; set; }
        /// <summary>
        /// 线程数
        /// </summary>
        [Display(Name = "线程数")]
        public int processthreadcount { get; set; }
        /// <summary>
        /// cpu时间
        /// </summary>
        [Display(Name = "cpu时间")]
        public decimal processcpuper { get; set; }
        /// <summary>
        /// 内存大小
        /// </summary>
        [Display(Name = "内存大小")]
        public decimal memorysize { get; set; }
        /// <summary>
        /// 文件大小
        /// </summary>
        [Display(Name = "文件大小")]
        public decimal filesize { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }
        /// <summary>
        /// 接口版本号
        /// </summary>
        [Display(Name = "接口版本号")]
        public decimal interfaceversion { get; set; }
        /// <summary>
        /// 协议json
        /// </summary>
        [Display(Name = "协议json")]
        public string protocoljson { get; set; }

    }	
}
﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_protocolversion
    public class tb_protocolversion
    {

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// 服务id
        /// </summary>
        [Display(Name = "服务id")]
        public int serviceid { get; set; }
        /// <summary>
        /// 协议版本号
        /// </summary>
        [Display(Name = "协议版本号")]
        public int version { get; set; }
        /// <summary>
        /// 接口版本号
        /// </summary>
        [Display(Name = "接口版本号")]
        public decimal interfaceversion { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }
        /// <summary>
        /// 协议json
        /// </summary>
        [Display(Name = "协议json")]
        public string protocoljson { get; set; }

    }

    //tb_protocolversion
    public class tb_protocolversion_search : BaseSearch
    {
        /// <summary>
        /// key
        /// </summary>
        [Display(Name = "key")]
        public string key { get; set; }
        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// 服务id
        /// </summary>
        [Display(Name = "服务id")]
        public int serviceid { get; set; }
        /// <summary>
        /// 协议版本号
        /// </summary>
        [Display(Name = "协议版本号")]
        public int version { get; set; }
        /// <summary>
        /// 接口版本号
        /// </summary>
        [Display(Name = "接口版本号")]
        public decimal interfaceversion { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }
        /// <summary>
        /// 协议json
        /// </summary>
        [Display(Name = "协议json")]
        public string protocoljson { get; set; }

    }
}
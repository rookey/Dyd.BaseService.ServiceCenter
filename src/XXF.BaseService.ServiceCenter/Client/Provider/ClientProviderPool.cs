﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Client.Provider
{
    /// <summary>
    /// 客户端提供者池
    /// </summary>
    public class ClientProviderPool
    {
        private static Dictionary<string, ClientProvider> _pool = new Dictionary<string, ClientProvider>();
        private static object _lockpool = new object();//连接池创建锁

        public ClientProvider Get(string servicenamespace)
        {
            string servicenamespacelower = servicenamespace.ToLower();
            if (_pool.ContainsKey(servicenamespacelower))
            {
                return _pool[servicenamespacelower] as ClientProvider;
            }
            lock (_lockpool)
            {
                if (_pool.ContainsKey(servicenamespacelower))
                {
                    return _pool[servicenamespacelower] as ClientProvider;
                }

                ClientProvider cp = new ClientProvider(servicenamespace);
                cp.Register();
                _pool.Add(servicenamespacelower, cp);
                return _pool[servicenamespacelower] as ClientProvider;
               
            }
        }

        public void Dispose()
        {
            lock (_lockpool)
            {
                foreach (var cp in _pool)
                {
                    try
                    {
                        cp.Value.Dispose();
                    }
                    catch { }
                }
                _pool.Clear();
            }
        }
       
    }
}

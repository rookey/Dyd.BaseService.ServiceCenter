
using System;
using System.Collections.Generic;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Test.Service
{
    /// <summary>
    /// 【类】测试实体2,【描述】我的测试实体2
    /// </summary> 
    public class MyEntity2 
    {
        
        /// <summary>
        /// 【属性】List,【描述】我的测试实体参数2
        /// </summary>     
        public List<int> P2 {get;set;}

        /// <summary>
        /// 【属性】实体参数2,【描述】我的测试实体参数3
        /// </summary>     
        public List<byte> P3 {get;set;}

        /// <summary>
        /// 【属性】实体参数2,【描述】我的测试实体参数3
        /// </summary>     
        public Dictionary<string,string> P4 {get;set;}

        /// <summary>
        /// 【属性】实体参数5,【描述】我的测试实体参数5
        /// </summary>     
        public bool P5 {get;set;}

        /// <summary>
        /// 【属性】实体参数6,【描述】我的测试实体参数6
        /// </summary>     
        public long P6 {get;set;}

        /// <summary>
        /// 【属性】实体参数6,【描述】我的测试实体参数6
        /// </summary>     
        public short P7 {get;set;}

        /// <summary>
        /// 【属性】实体参数6,【描述】我的测试实体参数6
        /// </summary>     
        public short P8 {get;set;}

        /// <summary>
        /// 【属性】实体参数6,【描述】我的测试实体参数6
        /// </summary>     
        public byte[] P9 {get;set;}

        /// <summary>
        /// 【属性】实体参数6,【描述】我的测试实体参数6
        /// </summary>     
        public string P10 {get;set;}

    }
}